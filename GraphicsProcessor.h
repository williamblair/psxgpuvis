#ifndef GRAPHICSPROCESSOR_H
#define GRAPHICSPROCESSOR_H

#include <cstdio>
#include <cstdint>
#include <sstream>
#include <vector>
#include <memory>

#include "Display.h"

class GraphicsProcessor {
public:
    GraphicsProcessor(std::shared_ptr<std::vector<std::string>> commandList,
                      std::shared_ptr<int> lineCounter);
    virtual ~GraphicsProcessor();
    
    /* 
     * Helper functions to parse the input strings and data 
     */
    void getColors(uint32_t data, uint8_t &B, uint8_t &G, uint8_t &R);
    void getNextData(uint32_t &data, std::string &gpuStr);
    
    /* 
     * Each GP must implement this function... 
     */
    virtual void parse(const Display &display, uint32_t data) = 0;
    
private:
    
    std::shared_ptr<std::vector<std::string>> m_commandList;
    std::shared_ptr<int> m_lineCounter;
    
};

#endif /* GRAPHICSPROCESSOR_H */

