#include "GP0.h"

GP0::GP0(std::shared_ptr<std::vector<std::string>> commandList,
         std::shared_ptr<int> lineCounter) 
: GraphicsProcessor(commandList, lineCounter)
{
    printf("In GP0 Constructor!\n");
}

GP0::~GP0() {
}

void GP0::parse(const Display &display, uint32_t data)
{
    /* Get which opcode it is */
    uint16_t command = (data >> 24) & 0xFF;
    
    switch(command)
    {
        case 0x00:  this->op_00(display, data); break;
        case 0xE1:  this->op_E1(display, data); break;
        case 0xE3:  this->op_E3(display, data); break;
        case 0xE4:  this->op_E4(display, data); break;
        default:
            printf("GP0: Unhandled command: 0x%08X\n", command);
            break;
    }
}

void GP0::op_00(const Display &display, uint32_t data)
{
    /* NOP! */
    printf("GP0: 0x00 Command: 0x%08X\n", data);
}

void GP0::op_E1(const Display &display, uint32_t data)
{
    printf("GP0: 0xE1 Command: 0x%08X\n", data);

    /* Texture page X Base */
    uint8_t xbase = data & 0xF;

    /* Texture page Y Base */
    uint8_t ybase = (data >> 4) & 1;

    printf("  X,Y Texture base: %d, %d\n", (int) xbase, (int) ybase);
    printf("  X*64: %d\n", (int) xbase * 64 );
    printf("  Y*256: %d\n", (int) ybase * 256 );

    /* Semi Transparency */
    uint8_t transparency = (data >> 5) & 0x3;
    printf("  Semi Transparency: %d\n", (int) transparency);

    /* Texture Page Colors */
    uint8_t tpagecolors = (data >> 7) & 0x3;
    printf("  Texture Page Colors: %d\n", (int) tpagecolors);

    /* Dither 24bit to 15bit? */
    uint8_t dither = (data >> 9) & 1;
    printf("  Dither 24bit to 15bit: %d\n", (int) dither);

    /* Drawing to display area? */
    uint8_t drawing = (data >> 10) & 1;
    printf("  Allow drawing to display area: %d\n", (int) drawing);

    /* Texture Disable? */
    uint8_t texdisable = (data >> 11) & 0x1;
    printf("  Texture disable if gp1(09h).Bit0=1: %d\n", (int) texdisable);

    /* Textured Rectangle X Flip? */
    uint8_t xflip = (data >> 12) & 0x1;
    printf("  Texture Rectangle X Flip: %d\n", (int) xflip);

    /* Textured Rectangle Y Flip? */
    uint8_t yflip = (data >> 13) & 0x1;
    printf("  Texture Rectangle X Flip: %d\n", (int) yflip);

    /* Command */
    // unnecessary!
}

void GP0::op_E3(const Display& display, uint32_t data)
{
    printf("GP0: 0xE3  Command: 0x%08X\n", data);

    /* X coordinate */
    uint16_t x = data & 0x3FF; // 0..1023

    /* Y coordinate (For old 160 pin GPU (max 1MB VRAM)) */
    uint16_t oldy = (data >> 10) & 0x1FF; // 0..511

    /* Y coordinate (For new 208 pin GPU (max 2MB VRAM)) */
    uint16_t y = (data >> 10) & 0x3FF; // 0..1023

    printf("  Drawing Area Top Left (old): %d,%d\n" , (int) x, (int) oldy);
    printf("  Drawing Area Top Left (new): %d,%d\n", (int) x, (int) y);
}

void GP0::op_E4(const Display& display, uint32_t data)
{
    printf("GP0: 0xE4  Command: 0x%08X\n", data);

    /* X coordinate */
    uint16_t x = data & 0x3FF; // 0..1023

    /* Y coordinate (For old 160 pin GPU (max 1MB VRAM)) */
    uint16_t oldy = (data >> 10) & 0x1FF; // 0..511

    /* Y coordinate (For new 208 pin GPU (max 2MB VRAM)) */
    uint16_t y = (data >> 10) & 0x3FF; // 0..1023

    printf("  Drawing Area Bottom Right (old): %d,%d\n", (int) x, (int) oldy);
    printf("  Drawing Area Bottom Right (new): %d,%d\n", (int) x, (int) y);
}

