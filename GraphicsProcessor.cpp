#include "GraphicsProcessor.h"

GraphicsProcessor::GraphicsProcessor(std::shared_ptr<std::vector<std::string>> commandList,
                                        std::shared_ptr<int> lineCounter)
: m_commandList(commandList),
  m_lineCounter(lineCounter)
{   
    printf("GP Constructor: Counter ref count: %ld\n", m_lineCounter.use_count());
}

GraphicsProcessor::~GraphicsProcessor() {
}

void GraphicsProcessor::getColors(uint32_t data, uint8_t &B, uint8_t &G, uint8_t &R)
{
    B = (data >> 16) & 0xFF;
    G = (data >> 8) & 0xFF;
    R = data & 0xFF;
}

void GraphicsProcessor::getNextData(uint32_t &data, std::string &gpuStr)
{   
    /* Initialize the stringstream with the line to parse */
    std::stringstream ss((*m_commandList)[*m_lineCounter]);
    (*m_lineCounter)++;
    
    /* Print the current line we're parsing? */
    printf("Cur line: %s\n", ss.str().c_str());
    
    /* Get which GPU the command is for */
    ss >> gpuStr;
    
    /* Get the command itself */
    ss >> std::hex >> data;
}