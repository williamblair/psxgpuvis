#ifndef GP0_H
#define GP0_H

#include <cstdio>
#include <cstdint>

#include "GraphicsProcessor.h"

class GP0 : public GraphicsProcessor {
public:
    GP0(std::shared_ptr<std::vector<std::string>> commandList,
        std::shared_ptr<int> lineCounter);
    virtual ~GP0();
    
    /*
     * Implementation of the GraphicsProcessor abstract function
     */
    void parse(const Display &display, uint32_t data);
    
private:

    /* 
     * Commands 
     */
    void op_00(const Display &display, uint32_t data); // NOP
    void op_E1(const Display &display, uint32_t data); // Draw mode setting (aka "Texpage")
    void op_E3(const Display &display, uint32_t data); // Set drawing area top left
    void op_E4(const Display &display, uint32_t data); // Set drawing area bottom right
};

#endif /* GP0_H */

