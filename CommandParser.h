#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H

#include <vector>
#include <string>
#include <cstdio>
#include <sstream>

#include "GP0.h"
#include "GP1.h"
#include "GraphicsProcessor.h"
#include "Display.h"

class CommandParser {
public:
    CommandParser(const std::vector<std::string> &commandList);
    virtual ~CommandParser();
    
    /*
     * Reads the next command, possibly over multiple lines
     */
    void parseNext(const Display &display);
    
private:
    
    std::shared_ptr<std::vector<std::string>> m_commandList;
    std::shared_ptr<int>         m_lineCounter;
    
    /* 
     * Contain functions for each respective command type 
     */
    GP0 m_gp0;
    GP1 m_gp1;
};

#endif /* COMMANDPARSER_H */

