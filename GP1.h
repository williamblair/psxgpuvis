#ifndef GP1_H
#define GP1_H

#include <cstdio>
#include <cstdint>

#include "GraphicsProcessor.h"

class GP1 : public GraphicsProcessor {
public:
    GP1(std::shared_ptr<std::vector<std::string>> commandList,
        std::shared_ptr<int> lineCounter);
    virtual ~GP1();
    
    void parse(const Display &display, uint32_t data);
private:
    
    /*
     * Commands
     */
    void op_00(const Display &display, uint32_t data); // Reset GPU
    void op_04(const Display &display, uint32_t data); // DMA direction / data request
    void op_08(const Display &display, uint32_t data); // Display mode

};

#endif /* GP1_H */

