#include "GP1.h"

GP1::GP1(std::shared_ptr<std::vector<std::string>> commandList,
        std::shared_ptr<int> lineCounter)
: GraphicsProcessor(commandList, lineCounter)
{
    printf("In GP1 constructor\n");
}

GP1::~GP1() {
}

void GP1::parse(const Display &display, uint32_t data)
{
    /* Get which opcode it is */
    uint16_t command = (data >> 24) & 0xFF;
    
    switch(command)
    {
        case 0x00: this->op_00(display, data); break;
        case 0x08: this->op_08(display, data); break;
        case 0x04: this->op_04(display, data); break;
        default:
            printf("GP1: Unhandled command: 0x%08X\n", command);
            break;
    }
}

void GP1::op_00(const Display& display, uint32_t data)
{
    printf("GP1: 0x00 Command: 0x%08X\n", data);
    
    /* Reset the GPU... 
     *
     * Resets the GPU to the following values:

  GP1(01h)      ;clear fifo
  GP1(02h)      ;ack irq (0)
  GP1(03h)      ;display off (1)
  GP1(04h)      ;dma off (0)
  GP1(05h)      ;display address (0)
  GP1(06h)      ;display x1,x2 (x1=200h, x2=200h+256*10)
  GP1(07h)      ;display y1,y2 (y1=010h, y2=010h+240)
  GP1(08h)      ;display mode 320x200 NTSC (0)
  GP0(E1h..E6h) ;rendering attributes (0)

Accordingly, GPUSTAT becomes 14802000h. The x1,y1 values are too small, ie. the upper-left edge isn't visible. Note that GP1(09h) is NOT affected by the reset command.
     */
}

void GP1::op_04(const Display& display, uint32_t data)
{
    printf("GP1 0x04 Command: 0x%08X\n", data);

    /* DMA Direction */
    uint8_t direction = data & 3;
    printf("  DMA Direction: %d\n", (int) direction);
}

void GP1::op_08(const Display &display, uint32_t data)
{
     printf("GP1 0x08 Command: 0x%08X\n", data);

    /* Horizontal Resolution 1 */
    uint8_t horizres = data & 0x3;
    printf("  Horizontal Resolution 1: %d\n", (int) horizres);

    /* Vertical Resolution */
    uint8_t vertres = (data >> 2) & 1;
    printf("  Vertical Resolution: %d\n", (int) vertres);

    /* Video Mode */
    uint8_t vidmode = (data >> 3) & 1;
    printf("  Video Mode: %d\n", (int) vidmode);

    /* Display Area Color Depth */
    uint8_t dispcolordepth = (data >> 4) & 1;
    printf("  Display Area Color Depth: %d\n", (int) dispcolordepth);

    /* Vertical Interlace */
    uint8_t interlaced = (data >> 5) & 1;
    printf("  Vertical Interlace: %d\n", (int) interlaced);

    /* Horizontal Resolution 2 */
    uint8_t horizres2 = (data >> 6) & 1;
    printf("  Horizontal Resolution 2: %d\n", (int) horizres2);

    /* Reverse Flag */
    uint8_t reverseflag = (data >> 7) & 1;
    printf("  Reverse Flag: %d\n", (int) reverseflag );
}

