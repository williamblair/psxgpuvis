#include "CommandParser.h"

CommandParser::CommandParser(const std::vector<std::string> &commandList)
: m_commandList(new std::vector<std::string>(commandList)),
  m_lineCounter(new int(0)),
  m_gp0(m_commandList, m_lineCounter),
  m_gp1(m_commandList, m_lineCounter)
{
    printf("Command List entry 0: %s\n", m_commandList->at(0).c_str());
    printf("Line Counter: %d, reference count: %ld\n", *m_lineCounter, 
            m_lineCounter.use_count());
}

CommandParser::~CommandParser() {
}

void CommandParser::parseNext(const Display &display)
{
    uint32_t data = 0;
    std::string gpu;
    
    /* It doesn't matter which GPU you use as they both do the same thing */
    m_gp0.getNextData(data, gpu);
    
    /* Send the command to the appropriate graphics processor */
    if (gpu == "GP0")   m_gp0.parse(display, data);
    else                m_gp1.parse(display, data);
}

