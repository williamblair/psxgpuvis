#include <cstdio>
#include <vector>
#include <string>

#include "Display.h"
#include "CommandParser.h"

#define S_WIDTH  1024
#define S_HEIGHT 480

std::vector<std::string> commands = {
    "GP0 0xe1003000",
    "GP0 0xe1003000",
    "GP0 0xe1003000",
    "GP0 0x0007924c",
    "GP1 0x00000000",
    "GP0 0xe1003000",
    "GP1 0x08000000",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0xe3000400",
    "GP0 0xe403c27f"
/*    "GP0 0xe5000800",
    "GP0 0xe100060a",
    "GP0 0xe2000000",
    "GP0 0xe6000000",
    "GP1 0x0503c400",
    "GP1 0x06c60260",
    "GP1 0x0703fc10",
    "GP1 0x08000003",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0x28000000",
    "GP0 0x00010000",
    "GP0 0x00010280",
    "GP0 0x00f10000",
    "GP0 0x00f10280",
    "GP1 0x04000000",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0xe303c400",
    "GP0 0xe407827f",
    "GP0 0xe5078800",
    "GP0 0xe100060a",
    "GP0 0xe2000000",
    "GP0 0xe6000000",
    "GP1 0x05000400",
    "GP1 0x06c60260",
    "GP1 0x0703fc10",
    "GP1 0x08000003",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0x28000000",
    "GP0 0x00f10000",
    "GP0 0x00f10280",
    "GP0 0x01e10000",
    "GP0 0x01e10280",
    "GP1 0x04000000",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0xe3000400",
    "GP0 0xe403c27f",
    "GP0 0xe5000800",
    "GP0 0xe100060a",
    "GP0 0xe2000000",
    "GP0 0xe6000000",
    "GP1 0x0503c400",
    "GP1 0x06c60260",
    "GP1 0x0703fc10",
    "GP1 0x08000003",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0x28000000",
    "GP0 0x00010000",
    "GP0 0x00010280",
    "GP0 0x00f10000",
    "GP0 0x00f10280",
    "GP1 0x04000000",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0xe303c400",
    "GP0 0xe407827f",
    "GP0 0xe5078800",
    "GP0 0xe100060a",
    "GP0 0xe2000000",
    "GP0 0xe6000000",
    "GP1 0x05000400",
    "GP1 0x06c60260",
    "GP1 0x0703fc10",
    "GP1 0x08000003",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0x28000000",
    "GP0 0x00f10000",
    "GP0 0x00f10280",
    "GP0 0x01e10000",
    "GP0 0x01e10280",
    "GP1 0x04000000",
    "GP0 0x0007924c",
    "GP1 0x00000000",
    "GP0 0xe1003000",
    "GP1 0x08000000",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0xe3000000",
    "GP0 0xe4077e7f",
    "GP0 0xe5000000",
    "GP0 0xe100020a",
    "GP0 0xe2000000",
    "GP0 0xe6000000",
    "GP1 0x05000800",
    "GP1 0x06c60260",
    "GP1 0x0703fc10",
    "GP1 0x08000027",
    "GP1 0x04000000",
    "GP1 0x04000002",
    "GP0 0x28000000",
    "GP0 0x00000000",
    "GP0 0x00000280",
    "GP0 0x01e00000" */ // line 109 in all_commands.txt
};

int main(int argc, char *argv[]) 
{
    Display display;
    if (!display.init(S_WIDTH, S_HEIGHT, "Hello World")) return -1;

    CommandParser commandParser(commands);
    
    for (int i = 0; i < commands.size(); ++i) {
        commandParser.parseNext(display);
    }
    
    display.show();
    
    return 0;
}

