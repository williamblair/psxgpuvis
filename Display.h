#ifndef DISPLAY_H
#define DISPLAY_H

#include "simple2d.h"

class Display {
public:
    Display();
    
    /* Don't allow copies please */
    Display(const Display& orig) = delete;
    
    virtual ~Display();
    
    bool init(const unsigned int width, const unsigned int height, 
                const char *title);
    
    void show(void);
private:

    static void update(void);
    static void render(void);
    
    S2D_Window *m_window;
};

#endif /* DISPLAY_H */

