#include "Display.h"

Display::Display() {
    m_window = NULL;
}

Display::~Display() {
}

bool Display::init(const unsigned int width, const unsigned int height, 
                    const char *title)
{
    m_window = S2D_CreateWindow(title, width, height, update, render, 0);
    
    return true;
}

void Display::show(void)
{
    S2D_Show(m_window);
}

void Display::update(void) {
    
}

void Display::render(void) {
    
    S2D_DrawTriangle(
            320, 50, 1, 0, 0, 1,
            540, 430, 0, 1, 0, 1,
            100, 430, 0, 0, 1, 1
        );    
    
}